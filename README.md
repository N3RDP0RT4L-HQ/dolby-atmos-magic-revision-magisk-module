# Dolby Atmos Magic Revision Magisk Module

## DISCLAIMER
- Dolby apps and blobs are owned by Dolby™.
- The MIT license specified here is for the Magisk Module, not for Dolby apps and blobs.

## Descriptions
- Dolby Surround soundfx equalizer ported and integrated as a Magisk Module for all supported and rooted devices with Magisk
- Global type soundfx

## Sources
- Dolby Magic Revision MD2 Magisk Module by Little Monk
- com.atmos classes: apkmirror.com com.dolby DAX1-1.9.3_r1 1

## Screenshots
- https://t.me/androidryukimodsdiscussions/78576

## Requirements
- Android 4.2 and up
- Magisk installed

## Installation Guide & Download Link
- Install this module https://www.pling.com/p/1610004/ via Magisk Manager or Recovery
- Install AML Magisk Module https://t.me/androidryukimodsdiscussions/29836 only if using any other audio mod module
- Reboot

## Optionals
- https://t.me/androidryukimodsdiscussions/60861
- https://t.me/androidryukimodsdiscussions/2616
- https://t.me/androidryukimodsdiscussions/26764

## Troubleshootings
- https://t.me/androidryukimodsdiscussions/29836
- https://t.me/androidryukimodsdiscussions/2617

## Support & Bug Report
- https://t.me/androidryukimodsdiscussions/2618

## Tested on
- Android 10 CrDroid ROM
- Android 11 DotOS ROM
- Android 12 AncientOS ROM
- Android 12.1 Nusantara ROM
- Android 13 AOSP ROM

## Credits and contributors
- https://t.me/viperatmos
- https://t.me/androidryukimodsdiscussions
- You can contribute ideas about this Magisk Module here: https://t.me/androidappsportdevelopment

## Thanks for Donations
- This Magisk Module is always will be free but you can however show us that you are care by making a donations:
- https://ko-fi.com/reiryuki
- https://www.paypal.me/reiryuki
- https://t.me/androidryukimodsdiscussions/2619


